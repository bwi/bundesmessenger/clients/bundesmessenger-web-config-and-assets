<div align="center">
  <img src="https://gitlab.opencode.de/bwi/bundesmessenger/info/-/raw/main/images/logo.png?inline=false" alt="BundesMessenger Logo" width="128" height="128">
</div>

<div align="center">
  <h2 align="center">BundesMessenger-Web Config and Assets</h2>
</div>

----

Wir freuen uns, dass Du Dich für den BundesMessenger interessierst.

## Grundsätzliches

BundesMessenger-Web Config and Assets ist ein unterstützendes Repository für Konfiguration im Rahmen des Build Prozesses.
Umgebungsspezifische Anpassungen an der config.json und assets werden hier abgelegt um diese innerhalb der Build Pipeline
auszutauschen oder zu erweitern. 

## Repo
https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web-config-and-assets

## Fehler und Verbesserungsvorschläge
https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web-config-and-assets/-/issues

## Abhängigkeiten
BundesMessenger Web - React SDK ( Prüfung ob Konfigurationen in Config und Assets konform zu Settings im React SDK sind )
BundesMessenger Web ( Austausch und Erweiterung der config.json und assets - siehe scripts/bwi/ build und package usw.. )

## Nutzung

Zur Erstellung einer eigenen Custom Config:

1. Umgebungsspezifische config.json unter /config anlegen z.B.: bundesmessenger.json
2. unter /assets gleichnamigen Ordner anlegen z.B.: bundesmessenger und zu überschreibende assets einfügen
3. config wird mit base config /base/base.json über jq zusammengefügt, wobei Werte aus der custom config Präzedenz haben.
4. Beim Build des Web Projektes wird für die custom config ein eigenes Artefakt erzeugt und die custom assets dort mit eingefügt. 


## Weitere Infos

Allgemeine Infos zum Thema BundesMessenger und was dahinter steckt findest Du [hier](https://gitlab.opencode.de/bwi/bundesmessenger/info) und alles weitere zur Web App findest Du unter [BundesMessenger Web](https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web).

### Copyright

- [BWI GmbH](https://messenger.bwi.de/copyright)
- [Element](https://element.io/copyright)
