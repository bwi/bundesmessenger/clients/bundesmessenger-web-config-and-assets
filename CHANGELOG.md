# Changes in BWI project 2.17.0 (2024-04-29)
===================================================

Changes in BWI project 2.16.0 (2024-03-21)
===================================================

Changes in BWI project 2.15.0 (2024-02-12)
===================================================

Changes in BWI project 2.14.0 (2024-01-15)
===================================================

Changes in BWI project 2.12.0 (2023-12-4)
===================================================

- Add federation flags
  - BwiFeature.hideE2Eshields (remove some e2ee shield icons)
  - BwiFeature.hideUserVerification (remove "Verify user" options in UserInfo panel)
  - BwiFeature.federation (enable federation ui elements)
  
Changes in BWI project 2.11.0 (2023-10-24)
===================================================

- MESSENGER-5182: Add BwiFeature.secureBackupSettings to enabled manual backup restore

Changes in BWI project 2.10.0 (2023-09-26)
===================================================

- nothing


Changes in BWI project 2.9.0 (2023-08-28)
===================================================

- nothing

Changes in BWI project 2.8.0 (2023-07-31)
===================================================

- MESSENGER-4959 Voice Broadcasting in BuM4BWI
- MESSENGER-4925 Huddle in BuM4BWI

Changes in BWI project 2.4.0 (2023-03-10)
===================================================

- MESSENGER-4273 added BWI config
- MESSENGER-4090 "UIFeature.shareQrCode": true
- Threads disabled

Changes in BWI project 1.25.0 (2022-09-20)
===================================================

- MESSENGER-3538: disabled live location sharing
- MESSENGER-3604: changed accent color
- MESSENGER-3609: added script and pipeline steps to validate bwi & nv feature flags
- default theme is now "bwi-light"
- MESSENGER-3404: added feature list link
- MESSENGER-3627: added copyright list link

Changes in BWI project 1.24.0 (2022-08-29)
===================================================

- added dev configs
- enabled undisclosed polls
- enabled mandatory crosssigning
- MESSENGER-3464: fixed default themes
- MESSENGER-3532: added 'de' & 'en' as the only languages
- MESSENGER-3243: suppress certain messages by default
- MESSENGER-3417: user should only be able to connect to configured mx server

Changes in BWI project 1.23.0 (2022-08-02)
===================================================

- no changes to prior version

