#!/bin/bash

# Copyright 2022 BWI GmbH
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -x

deforg="$1"
defrepo="$2"
defbranch="$3"

# use develop as default branch if explicitly set
[ -z "$defbranch" ] && defbranch="develop"

if [ -n "$CI_COMMIT_TAG" ]; then
    defbranch="master"

    cleanTag=`$(dirname $0)/normalize-version.sh ${CI_COMMIT_TAG}`
    if [[ $cleanTag =~ -rc.[0-9]{1,3}$ ]]; then
        releasebranch=$(cut -f1 -d"-" <<< $cleanTag)
        defbranch="release/$releasebranch"
    fi
fi

rm -r "$defrepo" || true

clone() {
    org=$1
    repo=$2
    branch=$3
    if [ -n "$branch" ]
    then
        echo "Trying to use $org/$repo#$branch"
        git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.example.com/$org/$repo.git $repo --branch "$branch" --depth 1 && exit 0
    fi
}

clone $deforg $defrepo $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
clone $deforg $defrepo $CI_COMMIT_BRANCH
clone $deforg $defrepo $defbranch
